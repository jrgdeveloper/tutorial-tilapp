import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
  
  router.get("hello") { req in
    return "hello!"
  }
  
  router.post("api", "acronyms") { req in
    return try req.content.decode(Acronym.self).flatMap { acronym in
      return acronym.save(on: req)
    }
  }
}
